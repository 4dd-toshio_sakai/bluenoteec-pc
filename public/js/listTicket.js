var listTicket = (function()
{

	jQuery.event.add( window, "load", function() {
		var category = new sliderObject( "#category_area" );
		var pickup = new sliderObject( "#pickup_area" );
		
		var arr = $(".item_panel");
		for( var i=0; i<arr.length; i++ ) {
			entryPanel( arr[i] );
		}
	});

	// ************************************************************************************
	// ************************************************************************************

	var entryPanel = function( div )
	{
		$(div).bind( "mouseover", function() {
			$(this).find(".item_panel_main_title").css("text-decoration", "underline" );
			$(this).find(".item_panel_sub_title").css("text-decoration", "underline" );
			$(this).find(".item_panel_pattern_img").css("opacity", 0.75 );
		});
		$(div).bind( "mouseout", function() {
			$(this).find(".item_panel_main_title").css("text-decoration", "none" );
			$(this).find(".item_panel_sub_title").css("text-decoration", "none" );
			$(this).find(".item_panel_pattern_img").css("opacity", 1.00 );
		});
	};


})();
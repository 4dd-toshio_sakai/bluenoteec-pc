var utility = (function()
{
	var imageView = function( num )
	{
		var arr = $(".floating_thum_block" );
		var imgsrc = $(arr[num]).find("img").attr("src");
		$("#floating_image_src").find("img").attr("src", imgsrc );

		$(arr).find(".floating_thum_focus").css( "opacity", 0 );
		$(arr[num]).find(".floating_thum_focus").css( "opacity", 1 );

		$("#floating").css( "display", "block" );
		$("#floating").stop();
		$("#floating").clearQueue();
		$("#floating").animate( {"opacity":1}, 200 );
	};

	var entryThum = function( div, num )
	{
		$(div).bind( "mouseover", function(){
			$(this).css("opacity", 0.5);
			$(this).stop();
			$(this).clearQueue();
			$(this).animate( {"opacity":1}, 300 )
		});

		$(div).bind( "click", function(){
			imageView( num );
		});
	};

	var resize = function()
	{
		var w = $(window).width();
		var h = $(window).height();

		$("#floating").width( w );
		$("#floating").height( h );

		if( h / 730  < 1 ) { 	
			//$("#floating_wrapper").css( "zoom", h / 730 );
			$("#floating_wrapper").attr( "class", "zoom75" );
		}
		else {
			$("#floating_wrapper").attr( "class", "zoom100" );
		}
	};

	var init = function()
	{
		var imgsrc = "";		
		resize();

		imgsrc = $( $("#item_detail_photo_main_zoom").find("a") ).find("img").attr("src");
		if( imgsrc != undefined ) {
			$("#floating_image_src").html( '<img src="'+imgsrc+'" />' );
		}

		imgsrc = $( $("#item_detail_photo_main").find("a") ).find("img").attr("src");
		if( imgsrc != undefined ) {
			$("#floating_image_src").html( '<img src="'+imgsrc+'" />' );
		}

		var cell1 = $(".item_detail_photo_menu_cell");
		for( var i=0; i<cell1.length; i++ ) {
			var img_src = $(cell1[i]).find("img").attr("src");
			var html = '';
			html += '<div class="floating_thum_block">';
			html += '<div class="floating_thum_cell"><img src="'+img_src+'" /></div>';
			html += '<span class="floating_thum_focus">&nbsp;</span>';
			html += '</div>';
			$("#floating_thum").append( html );
		}

		$("#floating_bt").find("a").bind("click", function(){
			$("#floating").stop();
			$("#floating").clearQueue();
			$("#floating").animate( {"opacity":0}, 200, function(){
				$("#floating").css( "display", "none" );
			} );
		});

		var arr = $(".floating_thum_block" );
		if( arr ) {
			for( var i=0; i < arr.length; i++ ) {
				entryThum( arr[i], i );
			}
		}

		
		var list = $("#header_user_list_menu");
		if( list ){
			$(list).bind("mouseover", function(){
				$("#header_user_list_menu_navi_arrow_df").css("display","none");	
				$("#header_user_list_menu_navi_arrow_ov").css("display","block");	
				$("#header_user_list_menu_navi_list").css("display","block");		
			});
			$(list).bind("mouseout", function(){
				$("#header_user_list_menu_navi_arrow_df").css("display","block");	
				$("#header_user_list_menu_navi_arrow_ov").css("display","none");	
				$("#header_user_list_menu_navi_list").css("display","none");	
			});
		}
	};

	jQuery.event.add( window, "resize",	function()	{	resize();	});
	jQuery.event.add( window, "load",	function() 	{	init();		});


	var ie8 = false;
	var ua = navigator.appVersion;
	if( ua.indexOf("MSIE") > 0 ) {
		if( ua.indexOf( "MSIE 8.0" ) > 0 ) {
			ie8 = true;
		}
	}
	
	var obj = {};

	obj.imageView = function( src )
	{
		var num = -1;
		var arr = $(".floating_thum_block" );
		for( var i=0; i<arr.length; i++ ) {
			var imgsrc = $(arr[i]).find("img").attr("src");
			if( src == imgsrc ) {
				num = i;
			}
		}

		if( num > -1 ) {
			imageView( num );
		}
	};

	obj.goPage = function(div) 
	{
		if( ie8 ) {
			window.location.href = "#"+div;
		}
		else {
			var dh = $("#"+div).offset().top - 50;
			$('html,body').animate( { "scrollTop":dh }, 250 );
		}
	};

	return obj;
})();
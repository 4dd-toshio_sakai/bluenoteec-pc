(function(){
	
	var ua = navigator.userAgent;
	
	if (localStorage 
		 && !localStorage.getItem("sp_flag") 
		 && (ua.indexOf('iPhone') > 0 || ua.indexOf('iPod') > 0 || ua.indexOf('Android') > 0 ) || ua.indexOf('Mobile') > 0 ) {
		if(confirm('スマートフォン用サイトを表示しますか？')) {
			var path=location.pathname;
			//console.log('hostname='+location.hostname+' pathname='+location.pathname +' search='+location.search+' target='+target);
			
			//rule01 /jp/sp/
			var target=path.slice( 4 );
			location.href = 'http://'+location.hostname+'/jp/sp/'+target+location.search;
			
			//rule02 /reserve/
			var target=path.slice( 9 );
			location.href = 'http://'+location.hostname+'/reserve/mb_'+target+location.search;
			
			//rule03 /ec/
			var target=path.slice( 4 );
			location.href = 'http://'+location.hostname+'/ec/mb_'+target+location.search;
		}else{
			localStorage.setItem("sp_flag",true);
		}
	}
})();
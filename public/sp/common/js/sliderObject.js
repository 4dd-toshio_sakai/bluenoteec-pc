function sliderObject( div ) 
{
	var sumHeight = 0;
	var blockNum = 0;

	var init = function() 
	{
		var target = $("#"+div);

		$(target).find(".blockarea_contents").css("height","auto");
		objectMax = $(target).find(".blockarea_contents").height();
		$(target).find(".blockarea_contents").css("height","0");


		$($(target).find(".blockarea_readmore")).find("a").bind("click", function(){
			readmore();
		});
		readmore();
	};

	var readmore = function()
	{
		var target = $("#"+div);
		for( var i = 0; i < 3; i++, blockNum++ ) {
			if( $(target).find(".blockarea_contents_cell_block").length > blockNum ) {
				sumHeight += $($(target).find(".blockarea_contents_cell_block")[blockNum]).outerHeight();
			}
		}
		if( $(target).find(".blockarea_contents_cell_block").length <= blockNum ) {
			$(target).find(".blockarea_readmore").css("display", "none");
		}

		$(target).find(".blockarea_contents").height(sumHeight);
	};

	init();

};
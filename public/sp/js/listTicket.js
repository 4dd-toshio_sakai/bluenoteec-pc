var listTicket = (function()
{
	var panelpos = 0;
	var panelmax = 0;
	var timer = null;

	var init = function()
	{
		$(".item_list_box").each( function(i){
			var parent = $(this);
			$(this).find(".item_list_box_inner").find( "a" ).bind( "click", function(){
				if( $(parent).find(".item_list_box_items_area").css( "display" ) == "block" ) {
					$(parent).find(".item_list_box_items_area").css( "display", "none" );
					$(parent).find(".item_list_box_link").attr( "class", "item_list_box_link item_list_box_open" );
				}
				else {
					$(parent).find(".item_list_box_items_area").css( "display", "block" );
					$(parent).find(".item_list_box_link").attr( "class", "item_list_box_link item_list_box_close" );
				}
			});
		});

		$("#item_list_search_header").find("a").bind( "click", function(){
			if( $("#item_list_search_body").css( "display" ) == "block" ) {
				$("#item_list_search_body").css( "display", "none" );
				$(this).attr( "class", "item_list_search_open" );
			}
			else {
				$("#item_list_search_body").css( "display", "block" );
				$(this).attr( "class", "item_list_search_close" );
			}
		});

		calcHeight();
	};


	var calcHeight = function()
	{
		if( timer ) {	clearInterval( timer );	}
		$("#pickup_area").find(".blockarea_contents").height( $("#pickup_area").find(".blockarea_contents_table_pickup").height() ); 
	};

	// ******************************************

	jQuery.event.add( window, "load", function() {
		init();
	});

	jQuery.event.add( window, "orientationchange", function() {
		timer = setInterval( calcHeight, 100 );
	});

})();
var controlHowto = (function()
{
	jQuery.event.add( window, "load", function() {
		var floatDestination = $("#cart_destination_float");
		if( floatDestination ) {
			$($(floatDestination).find(".cart_floating_wrapper")).css( "top", $("#cart_destination").offset().top + "px" );
		}

		var floatCard = $("#cart_card_float");
		if( floatCard ) {
			$($(floatCard).find(".cart_floating_wrapper")).css( "top", $("#cart_card").offset().top + "px" );
		}
	});


	// ----------------------------------------------------------------
	// ----------------------------------------------------------------
	// ----------------------------------------------------------------

	var obj = {};

	// ****************************************************

	obj.showDestinationForm = function( num ) {
		if( num == 1 ) {
			$("#cart_destination_info_1").css( "display", "none" );
			$("#cart_destination_form_1").css( "display", "block" );
		}
		if( num == 2 ) {
			$("#cart_destination_info_2").css( "display", "none" );
			$("#cart_destination_form_2").css( "display", "block" );
		}
		if( num == 3 ) {
			$("#cart_destination_info_3").css( "display", "none" );
			$("#cart_destination_form_3").css( "display", "block" );
		}
	};
	obj.saveDestinationForm	= function( num ) {
		if( num == 1 ) {
			$("#cart_destination_info_1").css( "display", "block" );
			$("#cart_destination_form_1").css( "display", "none" );
		}
		if( num == 2 ) {
			$("#cart_destination_info_2").css( "display", "block" );
			$("#cart_destination_form_2").css( "display", "none" );
		}
		if( num == 3 ) {
			$("#cart_destination_info_3").css( "display", "block" );
			$("#cart_destination_form_3").css( "display", "none" );
		}
	};
	obj.showDestinationFloating		= function() {
		var v_height = $(document).height();

		$("#cart_destination_float").stop();
		$("#cart_destination_float").clearQueue();
		$("#cart_destination_float").css("display", "block");
		$("#cart_destination_float").animate( {"opacity":1}, 500 );

		var floatDestination = $("#cart_destination_float");
		if( floatDestination ) {
			var v = 0;
			if ($.support['cssFloat']) { 
				v = $("#cart_destination").offset().top; 
			} else { 
				v = $("#cart_destination").offset().top + $("html:first").scrollTop(); 
			} 
			$($(floatDestination).find(".cart_floating_wrapper")).css( "top", v + "px" );
		}
		var v_new_top = 0;
		if ($.support['cssFloat']) { 
			v_new_top = $("#cart_destination").offset().top; 
		} else { 
			v_new_top = $("#cart_destination").offset().top + $("html:first").scrollTop(); 
		} 

		var f_new_height = $("#cart_destination_float").find(".cart_floating_wrapper").height();
		if( v_new_top+f_new_height > v_height ) {
			$("#cart_destination_float").height( v_new_top+f_new_height + "px" );
		}
		else {
			$("#cart_destination_float").height( v_height + "px" );
		}
	};
	obj.deleteDestinationFloating	= function() {
		$("#cart_destination_float").stop();
		$("#cart_destination_float").clearQueue();
		$("#cart_destination_float").animate( {"opacity":0}, 250, function(){
			$("#cart_destination_float").css("display", "none");
		} );
	};

	// ****************************************************

	obj.showCardForm 		= function( num ) {
		if( num == 1 ) {
			$("#cart_card_info_1").css( "display", "none" );
			$("#cart_card_form_1").css( "display", "block" );
		}
		if( num == 2 ) {
			$("#cart_card_info_2").css( "display", "none" );
			$("#cart_card_form_2").css( "display", "block" );
		}
		if( num == 3 ) {
			$("#cart_card_info_3").css( "display", "none" );
			$("#cart_card_form_3").css( "display", "block" );
		}
	};
	obj.saveCardForm 		= function( num ) {
		if( num == 1 ) {
			$("#cart_card_info_1").css( "display", "block" );
			$("#cart_card_form_1").css( "display", "none" );
		}
		if( num == 2 ) {
			$("#cart_card_info_2").css( "display", "block" );
			$("#cart_card_form_2").css( "display", "none" );
		}
		if( num == 3 ) {
			$("#cart_card_info_3").css( "display", "block" );
			$("#cart_card_form_3").css( "display", "none" );
		}
	};
	obj.showCardFloating	= function() {
		var v_height = $(document).height();

		$("#cart_card_float").stop();
		$("#cart_card_float").clearQueue();
		$("#cart_card_float").css("display", "block");
		$("#cart_card_float").animate( {"opacity":1}, 500 );

		var floatCard = $("#cart_card_float");
		if( floatCard ) {
			var v = 0;
			if ($.support['cssFloat']) { 
				v = $("#cart_card").offset().top; 
			} else { 
				v = $("#cart_card").offset().top + $("html:first").scrollTop(); 
			} 
			$($(floatCard).find(".cart_floating_wrapper")).css( "top", v+"px" );
		}
		var v_new_top = 0;
		if ($.support['cssFloat']) { 
			v_new_top = $("#cart_card").offset().top; 
		} else { 
			v_new_top = $("#cart_card").offset().top + $("html:first").scrollTop(); 
		} 
		var f_new_height = $("#cart_card_float").find(".cart_floating_wrapper").height();
		if( v_new_top+f_new_height > v_height ) {
			$("#cart_card_float").height( v_new_top+f_new_height + "px" );
		}
		else {
			$("#cart_card_float").height( v_height + "px" );
		}
	};
	obj.deleteCardFloating 	= function() {
		$("#cart_card_float").stop();
		$("#cart_card_float").clearQueue();
		$("#cart_card_float").animate( {"opacity":0}, 250, function(){
			$("#cart_card_float").css("display", "none");
		} );
	};

	// ****************************************************

	obj.showCommentForm	= function() {
		$("#cart_comment_form").css( "display", "block" );
		$("#cart_comment_info").css( "display", "none" );
	};
	obj.saveCommentForm	= function() {
		$("#cart_comment_form").css( "display", "none" );
		$("#cart_comment_info").css( "display", "block" );
	};
	
	// ****************************************************
	
	obj.showGiftForm	= function() {
		$("#cart_gift_info").css( "display", "none" );
		$("#cart_gift_form").css( "display", "block" );
	};
	obj.saveGiftForm	= function() {
		$("#cart_gift_info").css( "display", "block" );
		$("#cart_gift_form").css( "display", "none" );
	};

	// ----------------------------------------------------------------
	// ----------------------------------------------------------------
	// ----------------------------------------------------------------

	return obj;

})();
var special = (function()
{
	var sumWidth = 0;
	var blockWidth = 0;
	var blockNum = 0;

	var startX = 0;
	var initX = 0;

	var init = function()
	{
		for( var i = 0; i < $("#special_area").find(".blockarea_contents_cell_part").length; i++ ) {
			sumWidth += $($("#special_area").find(".blockarea_contents_cell_part")[i]).outerWidth();
			blockWidth = $($("#special_area").find(".blockarea_contents_cell_part")[i]).outerWidth();
		}

		$("#special_area").find(".blockarea_contents_slider").css( "width", sumWidth );

		$("#special_area").find(".blockarea_contents_slider").bind( "touchstart", function(e){
			var posX = $("#special_area").find(".blockarea_contents_slider").css( "left" );
			initX = parseFloat( posX.slice( 0, posX.indexOf("px",0) ) );
			var touchlist = e.originalEvent.touches;
			startX = touchlist[0].pageX * 2;
		});
		$("#special_area").find(".blockarea_contents_slider").bind( "touchmove", function(e){
			var touchlist = e.originalEvent.touches;
			var div = ( touchlist[0].pageX*2 - startX ) + initX;
			$("#special_area").find(".blockarea_contents_slider").css( "left", div );
		});

		$("#special_area").find(".blockarea_contents_slider").bind( "touchend", function(e){
			var posX = $("#special_area").find(".blockarea_contents_slider").css( "left" );
			var num = parseFloat( posX.slice( 0, posX.length-2 ) );

			var w = sumWidth-$("#special_area").find(".blockarea_contents").width()+5;

			if( num > 5 ) {
				$("#special_area").find(".blockarea_contents_slider").animate( {"left":5}, 100 );
			}
			if( Math.abs(num) > w ) {
				$("#special_area").find(".blockarea_contents_slider").animate( {"left":-w}, 100 );
			}
		});
	};

	// ******************************************

	jQuery.event.add( window, "load", function() {
		init();
	});

})();


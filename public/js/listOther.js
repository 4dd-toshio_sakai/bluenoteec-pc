var listOther = (function()
{

	jQuery.event.add( window, "load", function() {
		if( $("#category_area") ) {
			var category = new sliderObject( "#category_area" );
		}
		if( $("#pickup_area") ) {
			var pickup = new sliderObject( "#pickup_area" );
		}
		if( $("#other_area") ) {
			var other = new sliderObject( "#other_area" );
		}

		if( $("#cddvd_search_input") ) {
			$("#cddvd_search_input").bind( "focus",	function(){
				if( $(this).val() == "" ) {
					$("#cddvd_search_input_cover").css("display", "none");
				}
			});
			$("#cddvd_search_input").bind( "blur",	function(){
				if( $(this).val() == "" ) {
					$("#cddvd_search_input_cover").css("display", "block");
				}
			});
			$("#cddvd_search_input_cover").bind( "click", function(){
				$("#cddvd_search_input_cover").css("display", "none");
				$("#cddvd_search_input").focus();
			});
		}

		var arr = $(".item_panel");
		for( var i=0; i<arr.length; i++ ) {
			entryPanel( arr[i] );
		}
	});

	// ************************************************************************************
	// ************************************************************************************

	var entryPanel = function( div )
	{
		$(div).bind( "mouseover", function() {
			$(this).find(".item_panel_main_title").css("text-decoration", "underline" );
			$(this).find(".item_panel_sub_title").css("text-decoration", "underline" );
			$(this).find(".item_panel_pattern_img").css("opacity", 0.75 );
		});
		$(div).bind( "mouseout", function() {
			$(this).find(".item_panel_main_title").css("text-decoration", "none" );
			$(this).find(".item_panel_sub_title").css("text-decoration", "none" );
			$(this).find(".item_panel_pattern_img").css("opacity", 1.00 );
		});
	};

})();
var top = (function()
{
	var panelpos = 0;
	var panelmax = 0;
	var timer = null;

	var init = function()
	{
		var o1 = new sliderObject("ticket_area");
		var o2 = new sliderObject("cddvd_area");
		var o3 = new sliderObject("giftcard_area");
		var o4 = new sliderObject("goods_area");

		panelmax = $("#panel_navi_item_circle").find("a").length;

		$("#panel_navi_item_circle").find("a").each( function(i) {
			$(this).bind( "click", function() {	changePanel( i );	});
		});

		$("#panel_navi_item_arrow_left").find("a").each( function(i) {
			$(this).bind( "click", function() {	changePanel( panelpos-1 );	});
		});

		$("#panel_navi_item_arrow_right").find("a").each( function(i) {
			$(this).bind( "click", function() {	changePanel( panelpos+1 );	});
		});

		changePanel(0);
		resize();
	};

	var changePanel = function( panelnum ) 
	{
		if( panelnum < 0 ) {
			panelnum = panelmax-1;
		}
		if( panelmax <= panelnum ) {
			panelnum = 0;
		}

		clearInterval( timer );

		$($(".panel_screen_image")[panelpos]).css("display", "none");
		$($(".panel_screen_image")[panelpos]).css("opacity", 0 );
		$($("#panel_navi_item_circle").find("a")[panelpos]).removeClass("now");

		$($(".panel_screen_image")[panelnum]).css("display", "block");
		$($(".panel_screen_image")[panelnum]).animate({"opacity":1}, 200 );
		$($("#panel_navi_item_circle").find("a")[panelnum]).addClass("now");

		panelpos = panelnum;

		timer = setInterval( timerHandler, 5000 );
	};

	var timerHandler = function()
	{
		if( panelpos < panelmax-1 ) {
			changePanel( panelpos+1 );
		}
		else {
			changePanel(0);
		}
	};


	var resize = function()
	{
		var h = 180 / 320 * $(window).width();
		$("#panel_screen").height( h + "px" );
	};

	// ******************************************

	jQuery.event.add( window, "load", function() {
		init();
	});

	jQuery.event.add( window, "resize", function() {
		resize();
	});

})();
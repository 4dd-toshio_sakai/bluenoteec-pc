var header = (function()
{
	var sy = 0;
	var timer = null;

	var fadeDropDown = function()
	{
		clearInterval( timer );
		timer = null;
		$("#menu_dropdown").css("display","none");
	};

	var init = function()
	{
		scrollMove();
		sy = $("#header").height();

		$("#menu_cddvd").find("a").bind("mouseover", function(){
			if( timer ) {	clearInterval( timer );	timer = null;	} 
			$("#menu_dropdown").css("display","block");	
		});
		$("#menu_cddvd").find("a").bind("mouseout", function(){
			timer = setInterval( fadeDropDown, 100 );
		});

		$("#menu_dropdown").bind("mouseover", function(){
			if( timer ) {	clearInterval( timer );	timer = null;	} 
			$("#menu_dropdown").css("display","block");		
		});
		$("#menu_dropdown").bind("mouseout", function(){
			timer = setInterval( fadeDropDown, 100 );
		});
	};

	var scrollMove = function()
	{
		var top = $(window).scrollTop();
		if( top > sy ) {
			$("#menu").css("position", "fixed");
		}
		else {
			$("#menu").css("position", "relative");
		}
	};

	// ******************************************

	jQuery.event.add( window, "load", function() {
		init();
	});

	jQuery.event.add( window, "scroll", function() {
		scrollMove();
	});

})();
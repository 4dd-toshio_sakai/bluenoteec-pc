var detailOther = (function()
{
	var panelpos = 0;

	var init = function()
	{
		$("#item_detail_header_photo_menu").find("a").each( function(i) {
			$(this).bind( "click", function() {	changePanel( i );	});
		});

		changePanel(0);
	};


	var changePanel = function( panelnum ) 
	{
		$($(".item_detail_header_photo_main_block")[panelpos]).css("display", "none");
		$($(".item_detail_header_photo_main_block")[panelpos]).css("opacity", 0 );
		$($("#item_detail_header_photo_menu").find("a")[panelpos]).removeClass("now");

		$($(".item_detail_header_photo_main_block")[panelnum]).css("display", "block");
		$($(".item_detail_header_photo_main_block")[panelnum]).animate({"opacity":1}, 200 );
		$($("#item_detail_header_photo_menu").find("a")[panelnum]).addClass("now");

		panelpos = panelnum;
	};

	// ******************************************

	jQuery.event.add( window, "load", function() {
		init();
	});


})();
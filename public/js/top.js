var top = (function()
{

	// ************************************************************************************
	// ************************************************************************************
	// ************************************************************************************

	var pageNum = -1;
	var pageMax = 0
	var timer = null;

	var panelSlidePrev = function()
	{
		var num = pageNum - 1;
		if( -1 == num ) {
			num = pageMax - 1;
		}
		panelChange( num )
	};

	var panelSlideNext = function()
	{
		var num = pageNum + 1;
		if( pageMax == num ) {
			num = 0;
		}
		panelChange( num )
	};

	var panelChange = function( num ) 
	{
		var a = $(".panel_mainbanner_control_img").find("a");
		var img = $(".panel_mainbanner_area_img");

		if( pageNum != -1 ) {
			var tar = pageNum;
			$(a[tar]).stop();
			$(a[tar]).clearQueue();
			$(a[tar]).animate( {"opacity":0.0}, 250 );

			$(img[tar]).stop();
			$(img[tar]).clearQueue();
			$(img[tar]).animate( {"opacity":0.0}, 500, function(){
				$(img[tar]).css("display", "none");
			});
		}

		$(a[num]).stop();
		$(a[num]).clearQueue();
		$(a[num]).animate( {"opacity":1.0}, 250 );

		$(img[num]).css("display", "block");
		$(img[num]).stop();
		$(img[num]).clearQueue();
		$(img[num]).animate( {"opacity":1.0}, 500 );

		pageNum = num;

		clearInterval( timer );
		timer = setInterval( panelSlideNext, 5000 );
	};

	var panelEntryButton = function( div, num )
	{
		var a = $(div).find("a");

		$(a).bind( "mouseover",	function(){
			if( pageNum != num ) {
				$(a).stop();
				$(a).clearQueue();
				$(a).animate( {"opacity":0.5}, 250 );
			}
		});
		$(a).bind( "mouseout",	function(){
			if( pageNum != num ) {
				$(a).stop();
				$(a).clearQueue();
				$(a).animate( {"opacity":0.0}, 250 );
			}
		});
		$(a).bind( "click",		function(){
			panelChange( num );
		});
	};

	var panelInit = function() 
	{
		var ctl = $(".panel_mainbanner_control_img");
		if( ctl ) {
			pageMax = ctl.length;
			for( var i = 0; i < pageMax; i++ ) {
				panelEntryButton( ctl[i], i );
			}

			var lBt = $("#panel_mainbanner_control_left").find("a");
			$(lBt).bind( "mouseover",	function(){
				$(lBt).stop();
				$(lBt).clearQueue();
				$(lBt).animate( {"opacity":1.0}, 250 );
			});
			$(lBt).bind( "mouseout",	function(){
				$(lBt).stop();
				$(lBt).clearQueue();
				$(lBt).animate( {"opacity":0.5}, 250 );
			});
			$(lBt).bind( "click",		function(){
				panelSlidePrev();
			});
			$(lBt).animate( {"opacity":0.5}, 250 );

			var rBt = $("#panel_mainbanner_control_right").find("a");
			$(rBt).bind( "mouseover",	function(){
				$(rBt).stop();
				$(rBt).clearQueue();
				$(rBt).animate( {"opacity":1.0}, 250 );
			});
			$(rBt).bind( "mouseout",	function(){
				$(rBt).stop();
				$(rBt).clearQueue();
				$(rBt).animate( {"opacity":0.5}, 250 );
			});
			$(rBt).bind( "click",		function(){
				panelSlideNext();
			});
			$(rBt).animate( {"opacity":0.5}, 250 );
		}
		panelChange( 0 );
	};

	/*
	<div id="panel_mainbanner">
		<div id="panel_mainbanner_area">
			<p class="panel_mainbanner_area_img"><a href=""><img src="/img/main/banner_1.jpg" /></a></p>
			<p class="panel_mainbanner_area_img"><a href=""><img src="/img/main/banner_2.jpg" /></a></p>
			<p class="panel_mainbanner_area_img"><a href=""><img src="/img/main/banner_3.jpg" /></a></p>
			<p class="panel_mainbanner_area_img"><a href=""><img src="/img/main/banner_1.jpg" /></a></p>
			<p class="panel_mainbanner_area_img"><a href=""><img src="/img/main/banner_2.jpg" /></a></p>
		</div>
		<div id="panel_mainbanner_control_left"><a href="javascript:void(0);"><img src="/img/main/bt_panel_left.gif" /></a></div>
		<div id="panel_mainbanner_control_right"><a href="javascript:void(0);"><img src="/img/main/bt_panel_right.gif" /></a></div>
		<div id="panel_mainbanner_control">
			<p class="panel_mainbanner_control_img"><img src="/img/main/banner_1.jpg" /><a href="javascript:void(0);">&nbsp;</a></p>
			<p class="panel_mainbanner_control_img"><img src="/img/main/banner_2.jpg" /><a href="javascript:void(0);">&nbsp;</a></p>
			<p class="panel_mainbanner_control_img"><img src="/img/main/banner_3.jpg" /><a href="javascript:void(0);">&nbsp;</a></p>
			<p class="panel_mainbanner_control_img"><img src="/img/main/banner_1.jpg" /><a href="javascript:void(0);">&nbsp;</a></p>
			<p class="panel_mainbanner_control_img"><img src="/img/main/banner_2.jpg" /><a href="javascript:void(0);">&nbsp;</a></p>
		</div>
	</div>
	*/

	// ************************************************************************************
	// ************************************************************************************
	// ************************************************************************************

	jQuery.event.add( window, "load", function() {
		panelInit();
		
		var goods = new sliderObject("#goods_area");
		var cards = new sliderObject("#giftcard_area");
		var cddvd = new sliderObject("#cddvd_area");
		var ticket = new sliderObject("#ticket_area");
	});

})();


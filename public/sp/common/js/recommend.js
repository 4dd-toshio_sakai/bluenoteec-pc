var recommend = (function()
{
	var panelpos = 0;
	var panelmax = 0;
	var timer = null;

	var init = function()
	{
		$("#recommend_area").find(".blockarea_navi_item_circle").find("a").each( function(i) {
			$(this).bind( "click", function() {	changePanel( i );	});
		});
		$("#recommend_area").find(".blockarea_navi_item_arrow_left").find("a").each( function(i) {
			$(this).bind( "click", function() {	changePanel( panelpos-1 );	});
		});
		$("#recommend_area").find(".blockarea_navi_item_arrow_right").find("a").each( function(i) {
			$(this).bind( "click", function() {	changePanel( panelpos+1 );	});
		});

		panelmax = $("#recommend_area").find(".blockarea_contents_block").length;

		calcHeight();
	};

	var calcHeight = function()
	{
		if( timer ) {	clearInterval( timer );	}

		var w = $(".blockarea_contents_table_recommend").length * 100;
		$("#recommend_area").find(".blockarea_contents_slider").width( w + "%" );

		$("#recommend_area").find(".blockarea_contents_table_recommend").height( 0 );
		$("#recommend_area").find(".blockarea_contents").height( 0 );

		var h = 0;
		for( var i=0; i<$("#recommend_area").find(".blockarea_contents_table_recommend").length; i++ ) {
			$($("#recommend_area").find(".blockarea_contents_block")[i]).css("display", "block");
			$($("#recommend_area").find(".blockarea_contents_block")[i]).css("opacity", "1");
			if( h < $($("#recommend_area").find(".blockarea_contents_table_recommend")[i]).height() ){
				h = $($("#recommend_area").find(".blockarea_contents_table_recommend")[i]).height();
			}
			$($("#recommend_area").find(".blockarea_contents_block")[i]).css("display", "none");
			$($("#recommend_area").find(".blockarea_contents_block")[i]).css("opacity", "0");
		}

		$("#recommend_area").find(".blockarea_contents_table_recommend").height( h );
		$("#recommend_area").find(".blockarea_contents").height( h );

		changePanel( panelpos );
	};


	var changePanel = function( panelnum ) 
	{
		if( panelnum < 0 ) 			{	panelnum = panelmax-1;	}
		if( panelmax <= panelnum )	{	panelnum = 0;	}

		if( timer ) {	clearInterval( timer );	}

		$($("#recommend_area").find(".blockarea_contents_block")[panelpos]).css("display", "none");
		$($("#recommend_area").find(".blockarea_contents_block")[panelpos]).css("opacity", 0 );
		$($("#recommend_area").find(".blockarea_navi_item_circle").find("a")[panelpos]).removeClass("now");


		$($("#recommend_area").find(".blockarea_contents_block")[panelnum]).css("display", "block");
		$($("#recommend_area").find(".blockarea_contents_block")[panelnum]).animate({"opacity":1}, 200 );
		$($("#recommend_area").find(".blockarea_contents_block")[panelnum]).css("width", "100%");
		$($("#recommend_area").find(".blockarea_navi_item_circle").find("a")[panelnum]).addClass("now");

		var h = $($(".blockarea_contents_table_recommend")[panelnum]).height();
		$("#recommend_area").find(".blockarea_contents").height( h );
		
		panelpos = panelnum;
		timer = setInterval( timerHandler, 5000 );
	};


	var timerHandler = function()
	{
		changePanel( panelpos+1 );
	};




	// ******************************************

	jQuery.event.add( window, "load", function() {
		init();
	});

	// ******************************************

	jQuery.event.add( window, "orientationchange", function() {
		timer = setInterval( calcHeight, 100 );
	});

})();


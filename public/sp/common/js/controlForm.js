var controlForm = (function()
{
	function init()
	{
		$("#confirmCheck").bind("change", function(){
			var val = $("#confirmCheck:checked").val();

			if( val != undefined ) {
				$("#form_entry_submit_bt").find("img").css("display", "none");
				$("#form_entry_submit_bt").find("input").css("display", "block");
			}
			else {
				$("#form_entry_submit_bt").find("img").css("display", "block");
				$("#form_entry_submit_bt").find("input").css("display", "none");
			}
		})
	};

	jQuery.event.add( window, "load", function() 
	{
		init();
		$("#confirmCheck").attr( "checked", false );
		$("#form_entry_submit_bt").find("img").css("display", "block");
		$("#form_entry_submit_bt").find("input").css("display", "none");
	});

})();

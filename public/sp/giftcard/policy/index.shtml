<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title></title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<link rel="canonical" href="">

<!-- ////////////////////////////////////// -->
<!-- OG                                     -->
<!-- ////////////////////////////////////// -->
<meta property="og:title" content=""/>
<meta property="og:type" content=""/>
<meta property="og:locale" content="ja_JP" />
<meta property="og:description" content="" />
<!-- ////////////////////////////////////// -->
<!-- ////////////////////////////////////// -->
<!-- ////////////////////////////////////// -->

<!-- ////////////////////////////////////// -->
<!-- COMMON                                 -->
<!-- ////////////////////////////////////// -->
<link rel="stylesheet" type="text/css" href="/sp/common/css/common.css" media="screen,print" />
<link rel="stylesheet" type="text/css" href="/sp/common/css/header.css" media="screen,print" />
<link rel="stylesheet" type="text/css" href="/sp/common/css/menu.css" media="screen,print" />
<link rel="stylesheet" type="text/css" href="/sp/common/css/footer.css" media="screen,print" />
<script type="text/javascript" src="/common/js/jquery-1.7.min.js"></script>
<script type="text/javascript" src="/common/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="/sp/common/js/sliderObject.js"></script>
<script type="text/javascript" src="/sp/common/js/header.js"></script>
<script type="text/javascript" src="/sp/common/js/footer.js"></script>
<script type="text/javascript" src="/sp/common/js/special.js"></script>
<script type="text/javascript" src="/sp/common/js/recommend.js"></script>
<script type="text/javascript" src="/sp/common/js/pagetop.js"></script>
<script type="text/javascript" src="/sp/common/js/utility.js"></script>
<!-- ////////////////////////////////////// -->
<!-- ////////////////////////////////////// -->
<!-- ////////////////////////////////////// -->
</head>


<body>

<!-- SSI HEADER -->
<!--#include virtual="/sp/common/template/header_mini.shtml" -->


<div id="container">
	<div id="wrapper">
		<article>
			<h1 id="contents_title">ギフトカード利用規約</h1>
			<section>
				<div class="contents_block">
					<h2 class="contents_block_title">第１条（本約款の趣旨）</h2>
					<p class="contents_block_text_01">本約款は株式会社ブルーノート・ジャパン（以下「当社」と言います）が発行するギフトカード（以下「本カード」と言います）のご利用について規定するもので、本カードの所持者（以下「お客様」と言います）は本利用約款（以下「本約款」と言います）に従ってお取引いただくものとします。</p>
				</div>
			</section>
			
			<section>
				<div class="contents_block">
					<h2 class="contents_block_title">第２条（定義）</h2>
					<p class="contents_block_text_01">本約款において使用する語句の定義は、別途定義されない限り以下の通りとします。</p>
					<dl class="contents_block_list_01">
						<dt>
							<span class="contents_block_list_01_01">(1)</span>
							<span class="contents_block_list_01_02">ギフトカード</span>
						</dt>
						<dd>当社発行の前払式証票に類似した加減算型カードで、貨幣価値情報を電子データに変えて、入金することができ、また入金された金額をもってブルーノート東京、コットンクラブ、モーション・ブルー・ヨコハマの３店舗において商品を購入することができる機能を備えたもの（以下「本カード」と言う）を言います。</dd>
						<dt>
							<span class="contents_block_list_01_01">(2)</span>
							<span class="contents_block_list_01_02">お客様</span>
						</dt>
						<dd>本約款の規定の全部の内容を理解、同意及び承諾した上で、当社（当社から引き渡しを受けた第三者を含みます）から本カードの引き渡しを受け、現実に本カードを所持される方であって、本約款に規定された利用をされる方を言います。</dd>
						<dt>
							<span class="contents_block_list_01_01">(3)</span>
							<span class="contents_block_list_01_02">商品</span>
						</dt>
						<dd>当社各店にて販売する商品（ギフトカード、金券類、一部商品を除く）、または当社が提供する役務やサービスなどを総称したものを言います。</dd>
						<dt>
							<span class="contents_block_list_01_01">(4)</span>
							<span class="contents_block_list_01_02">ギフトカード取扱店</span>
						</dt>
						<dd>ブルーノート東京、コットンクラブ、モーション・ブルー・ヨコハマ（以下カード取扱店と言う）を言います。</dd>
						<dt>
							<span class="contents_block_list_01_01">(5)</span>
							<span class="contents_block_list_01_02">バリュー</span>
						</dt>
						<dd>お客様が商品の給付を当社に請求する上で必要となる流通貨幣に相当する価値をいいます。（なお、本カードの最終利用日現在における当該本カードのバリュー残余数量を、以下「残高」と言います）</dd>
						<dt>
							<span class="contents_block_list_01_01">(6)</span>
							<span class="contents_block_list_01_02">商品購入による利用</span>
						</dt>
						<dd>商品の給付を受けるために必要な残高を有する取扱店に提示することにより商品の給付を請求し、かつ当該商品の給付を受ける行為の総称を言います。</dd>
						<dt>
							<span class="contents_block_list_01_01">(7)</span>
							<span class="contents_block_list_01_02">入金</span>
						</dt>
						<dd>商品の給付を請求するために必要な残高を有しているか否かを問わず、本カードについて、本約款に規定する金額以上の金員を取扱店に対して支払うこと、または、その他の当社が指定する方法により、本カードの残高を増加させる行為を言います。</dd>
						<dt>
							<span class="contents_block_list_01_01">(8)</span>
							<span class="contents_block_list_01_02">残高照会</span>
						</dt>
						<dd>本カードを取扱店に提示することにより、またはその他の当社が指定する方法により、当該本カードのバリューの残高を確認する行為を言います。</dd>
						<dt>
							<span class="contents_block_list_01_01">(9)</span>
							<span class="contents_block_list_01_02">ご利用</span>
						</dt>
						<dd>本カードへの入金、本カードの商品購入による利用、および残高照会の行為を総称して「ご利用」と言います。</dd>
					</dl>
				</div>
			</section>
			
			<section>
				<div class="contents_block">
					<h2 class="contents_block_title">第３条（カードの発行）</h2>
					<p class="contents_block_text_01">当社は、取扱店において本カードを発行するものとし、お客様は、本約款に規定する金額以上の金員を取扱店に対してお支払いしていただくことにより、本カードの交付を受けることができるものとします。</p>
				</div>
			</section>
			
			<section>
				<div class="contents_block">
					<h2 class="contents_block_title">第４条（本カードのご利用）</h2>
					<p class="contents_block_text_01">お客様は、ブルーノート東京、コットンクラブ、モーション・ブルー・ヨコハマの３店舗においてのみ、本カードをご利用いただけるものとします。</p>
				</div>
			</section>
			
			<section>
				<div class="contents_block">
					<h2 class="contents_block_title">第５条（入金の方法）</h2>
					<ul class="contents_block_list_02">
						<li>本カードへの入金は、ブルーノート東京、コットンクラブ、モーション・ブルー・ヨコハマにて承るものとします。</li>
						<li>お客様は現金、またはクレジットカードにより、本カードへ入金することができるものとします。</li>
						<li>本カード１枚の１回あたりの入金は、当社が指定した金額以上となり、入金額がその指定された金額を超える場合には、1,000円単位での入金が可能です。</li>
						<li>本カードへ蓄積できる上限額は、本カード１枚につき金500,000円とします。</li>
					</ul>
				</div>
			</section>
			
			<section>
				<div class="contents_block">
					<h2 class="contents_block_title">第６条（利用の方法）</h2>
					<ul class="contents_block_list_02">
						<li>本カードにて商品をご購入される場合には、本カードの取扱店内のレジにて本カードをお渡しいただくものとします。本カードに入金された金額から商品購入合計額を差し引くことにより、金銭にてお支払いされた場合と同様の効果が生じます。この場合、商品購入合計額およびお支払い後の本カード残高はレシートに表示されますのでお客様はそれらに誤りが無いかを確認するものとする。</li>
						<li>お支払いの際に本カードを複数枚ご使用いただくこともできます。お渡しいただいた本カードの残高が商品合計金額に満たない場合は、不足分を現金、またはクレジットカードにてお支払いただくものとします。</li>
						<li>お客様は第６条各号の処理の後において、本カードにより利用された数量、および、当該利用後の当該本カードの残高を表示したレシートまたは明細書その他の当社が指定する書面（以下、単に「レシート」といいます）を取扱店から受け取るものとし、かつ当該レシートに記載された事項に過誤が無いことを確認しなければなりません。なお、この場合当該レシートの受取時にお客様から特定の申し出がない限り、お客様が当該記載された事項に過誤がないことを確認したものとみなすものとします。</li>
						<li>本カードを複数枚お持ちであっても、各本カード残高を1枚の本カードに統合することはできません。</li>
					</ul>
				</div>
			</section>
			
			<section>
				<div class="contents_block">
					<h2 class="contents_block_title">第７条（残高照会の方法）</h2>
					<ul class="contents_block_list_02">
						<li>お客様は、６条に規定するレシートによる残高照会のほか、取扱店において本カードを提示していただくことにより、本カードの残高照会をすることができます。</li>
						<li>当社の指定するウェブサイトにて残高を確認される場合は、本カード裏面に記載された１６桁のカード番号と裏面に記載された６桁のPIN番号（スクラッチ加工されている場合はコインなどでスクラッチ加工部分を軽くこすっていただくと６桁の数字が現れます）が必要です。</li>
						<li>当社の指定するウェブサイトにおいては、残高のほかご利用の履歴も確認が可能です。但し、システムの都合上、ウェブサイト上で表示することができる履歴内容、履歴件数は当社が決めるものとします。</li>
					</ul>
				</div>
			</section>
			
			<section>
				<div class="contents_block">
					<h2 class="contents_block_title">第８条（換金）</h2>
					<ul class="contents_block_list_02">
						<li>本カード自体または本カード残高の換金、返金及び払い戻し等はいたしません。</li>
						<li>但し、社会情勢の変化、関係法令等の制定改廃、その他当社の都合により本カードの取り扱いを全面的に廃止することを当社が決定した場合、例外的にお客様は当社に対して本カードの残高の払い戻しを求めることができるものとします。この場合、当社所定の方法に従って、本カードの残高照会の結果に基づき払い戻しを行うものといたします。</li>
						<li>前項の払い戻しを行った場合は、お客様より本カードを回収させていただくことといたします。</li>
					</ul>
				</div>
			</section>
			
			<section>
				<div class="contents_block">
					<h2 class="contents_block_title">第９条（再発行）</h2>
					<ul class="contents_block_list_02">
						<li>本カードを紛失した場合、もしくは盗難、改竄された場合、またはお客様の許可なく第三者に使用された場合であっても、本カードの機能停止、返金、または再発行はいたしません。</li>
						<li>カードやカードの機能を破損することがありますので、本カードを汚損したり、折り曲げたり、磁気に近づけたりしないで下さい。</li>
						<li>当社は本カードが毀損され、または、必要な機能が損なわれた場合、その原因がお客様の故意または過失に基づかないことが明らかであり、かつ当該本カード磁気情報または裏面に記載されている特定の番号が判読可能であるときに限り、当社の判断により、当社所定の方法に基づいて、必要な残高を利用する機能を備えた新たな本カードを発行するものとします。</dd>
						<li>その際、当社は新しい本カードと引き換えに旧本カードの引き渡しを求めることができるものとします。なお、新しいカードの発行にあたっては、本カードの図柄及び属性は当社が指定させていただくものとし、お客様は異議を述べないものといたします。</dd>
					</ul>
				</div>
			</section>
			
			<section>
				<div class="contents_block">
					<h2 class="contents_block_title">第１０条（不正な取得・利用等）</h2>
					<ul class="contents_block_list_02">
						<li>
							<p class="contents_block_list_02_01">次のいずれかに該当するときには、当社は、お客様に本カードのご利用をお断りし、本カード自体を無効扱いとしたうえで、お客様の本カードを当社に引渡しいただくものといたします。</p>
							<ul class="contents_block_list_03">
								<li>
									<span class="contents_block_list_03_01">①</span>
									<span class="contents_block_list_03_02">お客様が不正な方法により本カードを取得されたことを知って使用した場合や使用しようとした場合。</span>
								</li>
								<li>
									<span class="contents_block_list_03_01">②</span>
									<span class="contents_block_list_03_02">本カードが改竄、偽造、または変造されたものである場合。</span>
								</li>
								<li>
									<span class="contents_block_list_03_01">③</span>
									<span class="contents_block_list_03_02">本約款に違反した場合。</span>
								</li>
								<li>
									<span class="contents_block_list_03_01">④</span>
									<span class="contents_block_list_03_02">その他本カードの利用が不正であると当社が認める場合。</span>
								</li>
							</ul>
						</li>
						<li>当社が前各号の疑いがある場合、調査のため一時的に本カードをお預かりできるものとします。</li>
						<li>お客様は前１項の場合においても、払い戻しまたは本カードの再発行もしくは交換のいずれも一切請求することができないものとします。</li>
					</ul>
				</div>
			</section>

			<section>
				<div class="contents_block">
					<h2 class="contents_block_title">第１１条（ご利用期限に関する制限）【重要事項】</h2>
					<ul class="contents_block_list_02">
						<li>本カードを最後にご利用いただいた翌日を起算日として２年間ご利用がない場合、当該本カードは無効となり、当該本カードの残高の有無または多寡にかかわらず、返金等はしないことといたします。（ご利用とは、入金および本カードによる商品購入による利用をさします。）</li>
						<li>お客様は、前項の利用期限が到達した本カードに関するご利用および払い戻し等の請求権、一切に関することができないものとします。</li>
						<li>お客様は、本カードを長期間ご利用されない場合、十分に注意しなければいけないものとします。</li>
					</ul>
				</div>
			</section>

			<section>
				<div class="contents_block">
					<h2 class="contents_block_title">第１２条（システム保守・障害等）</h2>
					<p class="contents_block_text_01">本カードに関するシステムの設計、管理には万全を期しておりますが、停電、システム障害、メンテナンス、カード偽造等に関する管理、その他やむを得ない事情により本カード取扱店の一部または全店において、当社は予告なく本カードのご利用内容の一部またはすべてにつき一時的に停止できるものとします。その際、本カードがご利用いただけないことから不利益または障害が生じた場合でも、当社は一切の責任を負いません。</p>
				</div>
			</section>

			<section>
				<div class="contents_block">
					<h2 class="contents_block_title">第１３条（質権等担保権設定の禁止）</h2>
					<ul class="contents_block_list_02">
						<li>お客様は、本カードの質権等の担保設定を一切することができないものとします。</li>
						<li>当社はお客様が前項に違反した場合、当該違反から生じるトラブルに一切関与しないものとし、かつ一切責任を負わないこととします。</li>
					</ul>
				</div>
			</section>

			<section>
				<div class="contents_block">
					<h2 class="contents_block_title">第１４条（裁判管轄）</h2>
					<p class="contents_block_text_01">本約款に基づくご利用及び取引に関してお客様と当社または取扱店との間に紛争が生じた場合、当社の本店を管轄する簡易裁判所または地方裁判所を第一審の専属的合意管轄裁判所とするものとします。</p>
				</div>
			</section>

			<section>
				<div class="contents_block">
					<h2 class="contents_block_title">第１５条（本約款の変更）</h2>
					<p class="contents_block_text_01">本カードの利用その他の本約款に関するお問い合わせは、次の通りとするものとします。<br>附則：本約款は、平成26年10月1日からこれを適用します。</p>
					<p class="contents_block_text_01">《発行元》<br>株式会社ブルーノート・ジャパン<br>〒107-0062 東京都港区南青山6-3-16 ライカビル<br>TEL 03-5485-0088<a href="http://www.bluenote.co.jp" target="_brank" class="contents_block_list_02_02">http://www.bluenote.co.jp</a></p>
				</div>
			</section>
		</article>
	</div>
</div>

<!-- SSI FOOTER -->
<!--#include virtual="/sp/common/template/footer_mini.shtml" -->

</body>
</html>


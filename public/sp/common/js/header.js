var header = (function()
{
	var flag = true;

	var init = function()
	{
		$("#header_inner_navi_open").find("a").bind("click", function(){
			$("#header_menu").css( "display", "block" );
		});

		$("#header_menu_close").find("a").bind("click", function(){
			$("#header_menu").css( "display", "none" );
		});


		$("#header_status_info_link").bind("click", function(){
			if( flag ) {
				$("#header_status_info_menu").css( "display", "block" );
				$("#header_status_info_link").find("img").attr( "src", "/sp/common/img/header/bt_info_on.gif" );
				flag = false;
			}
			else {
				$("#header_status_info_menu").css( "display", "none" );
				$("#header_status_info_link").find("img").attr( "src", "/sp/common/img/header/bt_info_off.gif" );
				flag = true;
			}
		});
	};

	// ******************************************

	jQuery.event.add( window, "load", function() {
		init();
	});

})();
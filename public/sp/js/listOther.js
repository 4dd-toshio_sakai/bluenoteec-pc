var listOther = (function()
{
	var timer = null;

	var init = function()
	{
		$("#pickup_area").find(".blockarea_contents").height( $("#pickup_area").find(".blockarea_contents_table_pickup").height() ); 

		if( $("#item_list_search_form_parts_input") ) {
			$("#item_list_search_form_parts_input").bind( "focus", function(){
				$("#item_list_search_form_parts_image").css( "display", "none" );
			});
			$("#item_list_search_form_parts_input").bind( "blur", function(){
				if( $("#item_list_search_form_parts_input").val() == "" ) 
					$("#item_list_search_form_parts_image").css( "display", "block" );
			});
		}
	};

	var calcHeight = function()
	{
		if( timer ) {	clearInterval( timer );	}
		$("#pickup_area").find(".blockarea_contents").height( $("#pickup_area").find(".blockarea_contents_table_pickup").height() ); 
	};

	// ******************************************

	jQuery.event.add( window, "load", function() {
		init();
	});

	jQuery.event.add( window, "orientationchange", function() {
		timer = setInterval( calcHeight, 100 );
	});

})();

var utility = (function()
{
	var init = function()
	{
	};

	// ******************************************

	var obj = {};

	obj.goPage = function(div) 
	{
		var dh = $("#"+div).offset().top;
		$('html,body').animate( { "scrollTop":dh }, 250 );
	};

	return obj;

	// ******************************************

	jQuery.event.add( window, "load", function() {
		init();
	});

})();
function specialObject( id )
{
	var target = "#" + id;
	var sumWidth = 0;
	var blockWidth = 0;
	var blockNum = 0;
	var startX = 0;
	var initX = 0;

	var init = function()
	{
		for( var i = 0; i < $(target).find(".blockarea_contents_cell_part").length; i++ ) {
			sumWidth += $($(target).find(".blockarea_contents_cell_part")[i]).outerWidth();
			blockWidth = $($(target).find(".blockarea_contents_cell_part")[i]).outerWidth();
		}

		$(target).find(".blockarea_contents_slider").css( "width", sumWidth );
		$(target).find(".blockarea_contents_slider").bind( "touchstart", function(e){
			var posX = $(target).find(".blockarea_contents_slider").css( "left" );
			initX = parseFloat( posX.slice( 0, posX.indexOf("px",0) ) );
			var touchlist = e.originalEvent.touches;
			startX = touchlist[0].pageX * 2;
		});
		$(target).find(".blockarea_contents_slider").bind( "touchmove", function(e){
			var touchlist = e.originalEvent.touches;
			var div = ( touchlist[0].pageX*2 - startX ) + initX;
			$(target).find(".blockarea_contents_slider").css( "left", div );
		});

		$(target).find(".blockarea_contents_slider").bind( "touchend", function(e){
			var posX	= $(target).find(".blockarea_contents_slider").css( "left" );
			var num		= parseFloat( posX.slice( 0, posX.length-2 ) );
			var w		= sumWidth-$(target).find(".blockarea_contents").width()+5;

			if( num > 5 ) {
				$(target).find(".blockarea_contents_slider").animate( {"left":5}, 100 );
			}
			if( Math.abs(num) > w ) {
				$(target).find(".blockarea_contents_slider").animate( {"left":-w}, 100 );
			}
		});
	};

	init();
};


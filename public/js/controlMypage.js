var controlMypage = (function()
{
	jQuery.event.add( window, "load", function() {
		var floatDestination1 = $("#mypage_address_float");
		if( floatDestination1 ) {
			$($(floatDestination1).find(".mypage_floating_wrapper")).css( "top", $("#mypage_address").offset().top + "px" );
		}

		var floatDestination2 = $("#mypage_card_float");
		if( floatDestination2 ) {
			$($(floatDestination2).find(".mypage_floating_wrapper")).css( "top", $("#mypage_card").offset().top + "px" );
		}
	});

	// ----------------------------------------------------------------
	// ----------------------------------------------------------------

	var obj = {};

	// ----------------------------------------------------------------
	// ----------------------------------------------------------------

	obj.showInfoForm		= function() {
		$("#mypage_info_confirm").css( "display", "none" );
		$("#mypage_info_form").css( "display", "block" );
	};
	obj.showAddressForm		= function( num ) {
		if( num == 1 ) {
			$("#mypage_address_confirm_1").css( "display", "none" );
			$("#mypage_address_form_1").css( "display", "block" );
		}
		if( num == 2 ) {
			$("#mypage_address_confirm_2").css( "display", "none" );
			$("#mypage_address_form_2").css( "display", "block" );
		}
		if( num == 3 ) {
			$("#mypage_address_confirm_3").css( "display", "none" );
			$("#mypage_address_form_3").css( "display", "block" );
		}
	};
	obj.showCardForm		= function( num ) {
		if( num == 1 ) {
			$("#mypage_card_confirm_1").css( "display", "none" );
			$("#mypage_card_form_1").css( "display", "block" );
		}
		if( num == 2 ) {
			$("#mypage_card_confirm_2").css( "display", "none" );
			$("#mypage_card_form_2").css( "display", "block" );
		}
		if( num == 3 ) {
			$("#mypage_card_confirm_3").css( "display", "none" );
			$("#mypage_card_form_3").css( "display", "block" );
		}
	};
	obj.showAboutForm		= function() {
		$("#mypage_about_confirm").css( "display", "none" );
		$("#mypage_about_form").css( "display", "block" );
	};

	// ----------------------------------------------------------------
	
	obj.showAddressFloating		= function() {
		$("#mypage_address_float").stop();
		$("#mypage_address_float").clearQueue();
		$("#mypage_address_float").css("display", "block");
		$("#mypage_address_float").animate( {"opacity":1}, 500 );

		var floatDestination1 = $("#mypage_address_float");
		if( floatDestination1 ) {
			var v = 0;
			if ($.support['cssFloat']) { 
				v = $("#mypage_address").offset().top; 
			} else { 
				v = $("#mypage_address").offset().top + $("html:first").scrollTop(); 
			} 

			$($(floatDestination1).find(".mypage_floating_wrapper")).css( "top", v + "px" );
		}
		var v_new_top = 0;
		if ($.support['cssFloat']) { 
			v_new_top = $("#mypage_address").offset().top; 
		} else { 
			v_new_top = $("#mypage_address").offset().top + $("html:first").scrollTop(); 
		} 

		var f_new_height = $("#mypage_address").find(".mypage_floating_wrapper").height()
		if( v_new_top+f_new_height > $(document).height() ) {
			var hD = $("#mypage_address_float").find(".mypage_floating_bg").height() + v_new_top;
			$("#mypage_address_float").height( hD + "px" );
		}
	};
	obj.deleteAddressFloating		= function() {
		$("#mypage_address_float").stop();
		$("#mypage_address_float").clearQueue();
		$("#mypage_address_float").animate( {"opacity":0}, 250, function(){
			$("#mypage_address_float").css("display", "none");
		} );
	};

	// ----------------------------------------------------------------
	
	obj.showCardFloating		= function() {
		$("#mypage_card_float").stop();
		$("#mypage_card_float").clearQueue();
		$("#mypage_card_float").css("display", "block");
		$("#mypage_card_float").animate( {"opacity":1}, 500 );

		var floatDestination2 = $("#mypage_card_float");
		if( floatDestination2 ) {
			var v = 0;
			if ($.support['cssFloat']) { 
				v = $("#mypage_card").offset().top; 
			} else { 
				v = $("#mypage_card").offset().top + $("html:first").scrollTop(); 
			} 
			$($(floatDestination2).find(".mypage_floating_wrapper")).css( "top", v + "px" );
		}
		var v_new_top = 0;
		if ($.support['cssFloat']) { 
			v_new_top = $("#mypage_card").offset().top; 
		} else { 
			v_new_top = $("#mypage_card").offset().top + $("html:first").scrollTop(); 
		} 

		var f_new_height = $("#mypage_card_float").find(".mypage_floating_wrapper").height()
		if( v_new_top+f_new_height > $(document).height() ) {
			var hD = $("#mypage_card_float").find(".mypage_floating_bg").height() + v_new_top;
			$("#mypage_card_float").height( hD + "px" );
		}
	};

	obj.deleteCardFloating		= function() {
		$("#mypage_card_float").stop();
		$("#mypage_card_float").clearQueue();
		$("#mypage_card_float").animate( {"opacity":0}, 250, function(){
			$("#mypage_card_float").css("display", "none");
		} );
	};

	// ----------------------------------------------------------------
	// ----------------------------------------------------------------

	return obj;

	// ----------------------------------------------------------------
	// ----------------------------------------------------------------

})();
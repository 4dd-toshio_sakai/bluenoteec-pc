var detailTicket = (function()
{
	var panelpos = 0;

	var init = function()
	{
		$("#item_detail_header_photo_menu").find("a").each( function(i) {
			$(this).bind( "click", function() {	changePanel( i );	});
		});

		changePanel(0);
	};


	var changePanel = function( panelnum ) 
	{
		$($(".item_detail_header_photo_main_block")[panelpos]).css("display", "none");
		$($(".item_detail_header_photo_main_block")[panelpos]).css("opacity", 0 );
		$($("#item_detail_header_photo_menu").find("a")[panelpos]).removeClass("now");

		$($(".item_detail_header_photo_main_block")[panelnum]).css("display", "block");
		$($(".item_detail_header_photo_main_block")[panelnum]).animate({"opacity":1}, 200 );
		$($("#item_detail_header_photo_menu").find("a")[panelnum]).addClass("now");

		panelpos = panelnum;
	};

	// ******************************************

	jQuery.event.add( window, "load", function() {
		init();
	});

	// ******************************************

	var obj = {};

	obj.openAndClose = function( id ) 
	{
		if( $("#"+id).find(".item_detail_ticket_items").css("display") == "block" ) {
			$("#"+id).find(".item_detail_ticket_info_status_open").css("display", "table-cell");
			$("#"+id).find(".item_detail_ticket_info_status_close").css("display", "none");

			$("#"+id).find(".item_detail_ticket_items").css("display", "none");
			$("#"+id).find(".item_detail_ticket_wrap_close").attr( "class", "item_detail_ticket_wrap_open" );

		}
		else {
			$("#"+id).find(".item_detail_ticket_info_status_open").css("display", "none");
			$("#"+id).find(".item_detail_ticket_info_status_close").css("display", "table-cell");

			$("#"+id).find(".item_detail_ticket_items").css("display", "block");
			$("#"+id).find(".item_detail_ticket_wrap_open").attr( "class", "item_detail_ticket_wrap_close" );
		}

	};

	obj.openTicket = function() 
	{
		$("#item_detail_ticket_items").css("display", "block");
		$("#item_detail_ticket_about").css("display", "none");

		$( $("#item_detail_type_inner").find("a")[0] ).attr( "class", "item_detail_type_inner_here" );
		$( $("#item_detail_type_inner").find("a")[1] ).attr( "class", "" );
	};

	obj.openAbout = function() 
	{
		$("#item_detail_ticket_items").css("display", "none");
		$("#item_detail_ticket_about").css("display", "block");

		$( $("#item_detail_type_inner").find("a")[0] ).attr( "class", "" );
		$( $("#item_detail_type_inner").find("a")[1] ).attr( "class", "item_detail_type_inner_here" );
	};

	return obj;

})();
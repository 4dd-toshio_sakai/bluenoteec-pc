var detailOther = (function()
{
	var _maxnum = 0;
	var _page = 0;

	var subScroll = function( num )
	{
		if( num * 3  <= _maxnum ) {
			if( num >= 0 ) {
				_page = num;
				$("#item_sub_slider").animate( {"left":-(930*_page)+"px"}, 250 );
				$($(".item_subtitle_status_icon").find("span")).attr( "class", "item_subtitle_status_bar" );
				$($(".item_subtitle_status_icon").find("span")[_page]).attr( "class", "item_subtitle_status_bar_here" );
			}
		}
	};

	var subEntryButton = function( div )
	{
		var a = $(div).find(".panel_link");

		$(a).bind( "mouseover",	function(){
			$(div).stop();
			$(div).clearQueue();
			$(div).css( "opacity", 0.50 );
			$(div).animate( {"opacity":1.00}, 250, "linear" );
			$(div).find(".panel_title").addClass("text_underline");
			$(div).find(".panel_name").addClass("text_underline");
		});
		$(a).bind( "mouseout",	function(){
			$(div).find(".panel_title").removeClass("text_underline");
			$(div).find(".panel_name").removeClass("text_underline");
		});
	};

	var subNaviButton = function( div, num )
	{
		$(div).bind( "mouseover",	function(){
			$(div).stop();
			$(div).clearQueue();
			$(div).css( "opacity", 0.50 );
			$(div).animate( {"opacity":1.00}, 250, "linear" );
		});
		$(div).bind( "mouseout",	function(){
		});
		$(div).bind( "click",		function(){
			subScroll( num );
		});
	};

	var subInit = function()
	{
		var arr = $(".sub_panel");
		_maxnum = arr.length;
		if( _maxnum > 0 )	{ 
			for( var i=0; i<_maxnum; i++ ) {
				subEntryButton( arr[i] );
			}
		}

		if( _maxnum >= 3 ) {
			$("#item_subtitle_status").html( "" );
			// **********************************************************************************
			for( var i=1; (i-1)*3 < _maxnum; i++ ) {
				$("#item_subtitle_status").append( '<p class="item_subtitle_status_icon"><span class="item_subtitle_status_bar">&nbsp;</span></p>' );
			}
			for( var i=0; i < $(".item_subtitle_status_icon").length; i++ ) {
				subNaviButton( $(".item_subtitle_status_icon")[i], i );
			}
			subScroll( 0 );
		}
	};

	// ************************************************************************************
	// ************************************************************************************

	var formEntryChange = function( div )
	{
		$(div).find("select").bind( "change",		function(){
			var val = $(this).val();
			if( val == "-" ) {
				$(div).find(".item_detail_info_table_item_count_form_image").css( "display", "inline-block" );
				$(div).find(".item_detail_info_table_item_count_form_submit").css( "display", "none" );
			}
			else {
				$(div).find(".item_detail_info_table_item_count_form_image").css( "display", "none" );
				$(div).find(".item_detail_info_table_item_count_form_submit").css( "display", "inline-block" );
			}
		});
	};

	var formInit = function()
	{
		var arr = $(".item_detail_info_table_num_form");
		if( arr ) {
			for( var i=0; i < arr.length; i++ ) {
				formEntryChange( arr[i] );
			}
		}
	};

	// ************************************************************************************
	// ************************************************************************************

	var photoChange = function( div )
	{
		var src = $(div).find("img").attr("src")

		var a = $("#item_detail_photo_main").find("a");
		$(a).find("img").attr( "src", src );
		
		$(".item_detail_photo_menu_cell_focus").css( "opacity", 0 );
		$(div).find(".item_detail_photo_menu_cell_focus").stop();
		$(div).find(".item_detail_photo_menu_cell_focus").clearQueue();
		$(div).find(".item_detail_photo_menu_cell_focus").animate( {"opacity":1}, 250 );
	};

	var photoEntryButton = function( div )
	{
		$(div).bind( "mouseover",	function(){
			$(div).stop();
			$(div).clearQueue();
			$(div).animate( {"opacity":0.50}, 250, "linear" );
		});
		$(div).bind( "mouseout",	function(){
			$(div).stop();
			$(div).clearQueue();
			$(div).animate( {"opacity":1.00}, 100, "linear" );
		});
		$(div).bind( "click",		function(){
			photoChange( div );
		});
	};

	var photoInit = function()
	{
		var arr = $(".item_detail_photo_menu_cell");
		if( arr )	{
			for( var i=0; i<arr.length; i++ ) {
				photoEntryButton( arr[i] );
			}
			photoChange( arr[0] );
		}

		var photoblock = $("#item_detail_photo_main");
		$(photoblock).bind( "mouseover",	function(){
			$(photoblock).css( "opacity", 0.5 );
			$(photoblock).stop();
			$(photoblock).clearQueue();
			$(photoblock).animate( {"opacity":1.00}, 500, "linear" );
		});

		$(photoblock).bind( "click",		function(){
			var a = $("#item_detail_photo_main").find("a");
			var img = $(a).find("img").attr( "src" );
			var imgsrc = $(a).find("img").attr( "src" );
			utility.imageView( imgsrc );
		});
	};

	// ************************************************************************************
	// ************************************************************************************

	jQuery.event.add( window, "load", function() {
		formInit();
		subInit();
		photoInit();
		
		if( $("#related_area") ) {
			var relatedArea = new sliderObject("#related_area");
		}
		
		if( $("#other_area") ) {
			var otherArea = new sliderObject("#other_area");
		}

	});

})();
var controlForm = (function()
{

	function init()
	{
		$("#confirmCheck").bind("change", function(){
			var val = $("#confirmCheck:checked").val();
			if( val != undefined ) {
				$($(".form_entry_check_button_inner")[0]).css("display", "none");
				$($(".form_entry_check_button_inner")[1]).css("display", "block");
			}
			else {
				$($(".form_entry_check_button_inner")[0]).css("display", "block");
				$($(".form_entry_check_button_inner")[1]).css("display", "none");
			}
		})
	};

	jQuery.event.add( window, "load", function() {
		init();

		$("#confirmCheck").attr( "checked", false );
	});

})();

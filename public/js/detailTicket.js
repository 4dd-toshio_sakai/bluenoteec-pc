var detailTicket = (function()
{
	// ************************************************************************************
	// ************************************************************************************
	var resize = function()
	{
		var w = $(window).width();
		var h = $(window).height();

		$("#map_floating").width( w );
		$("#map_floating").height( h );

		if( h / 730  < 1 ) { 	
			//$("#map_wrapper_floating").css( "zoom", h / 730 );
			$("#map_wrapper_floating").attr( "class", "zoom75" );
		}
		else {
			//$("#map_wrapper_floating").css( "zoom", 1 );
			$("#map_wrapper_floating").attr( "class", "zoom100" );
		}
	};

	var imageView = function( imgsrc )
	{
		resize();
		
		$("#map_image_src_floating").html( '<img src="'+imgsrc+'" />' );

		$("#map_floating").css( "display", "block" );
		$("#map_floating").stop();
		$("#map_floating").clearQueue();
		$("#map_floating").animate( {"opacity":1}, 200 );
	};


	var entryPhoto = function( div ) {
		var img = $("#item_detail_photo_main").find("a");

		img.bind("click", function(){
			var imgsrc = $(this).attr( "src" );
			imageView( imgsrc );
		});
		img.bind("mouseover", function(){
			$(img).stop();
			$(img).clearQueue();
			$(img).css( "opacity", 0.50 );
			$(img).animate( {"opacity":1.00}, 250, "linear" );
		});

		var focus = $(".item_detail_photo_menu_cell");
		focus.bind("mouseover", function(){
			var cover = $(this).find(".item_detail_photo_menu_cell_focus");
			$(cover).stop();
			$(cover).clearQueue();
			$(cover).animate( {"opacity":0.50}, 150, "linear" );
		});
	};

	var entryTip = function( div ) {
		var form = $(div).find(".item_detail_ticket_table_item_count_form");

		$(form).bind( "change", function() {
			if( $(form).find("select[name='form_cart']").val() == "-" ) {
				$(form).find(".item_detail_ticket_table_item_count_form_submit").css("display","none");
				$(form).find(".item_detail_ticket_table_item_count_form_image").css("display","block");
			}
			else {
				$(form).find(".item_detail_ticket_table_item_count_form_submit").css("display","block");
				$(form).find(".item_detail_ticket_table_item_count_form_image").css("display","none");
			}
		});
	};

	var entryButton = function( div ) {
		var list = $(div).find(".item_detail_ticket_list");
		var wrap = $(div).find(".item_detail_ticket_wrap");
		var title = $(div).find(".item_detail_ticket_table_title");
		var item = $(div).find(".item_detail_ticket_table_item");
		var info = $(div).find(".item_detail_ticket_table_info");
		var img = $(div).find(".item_detail_ticket_table_info_map_img").find("a");

		img.bind("click", function(){
			var imgsrc = $(this).find("img").attr("src");
			imageView( imgsrc );
		});

		img.bind("mouseover", function(){
			$(this).stop();
			$(this).clearQueue();
			$(this).css( "opacity", 0.50 );
			$(this).animate( {"opacity":1.00}, 250, "linear" );
		});

		list.bind("click", function() {
			var pos_h = 0;
			var tar_h = 0;

			if( $(div).hasClass("item_detail_ticket_close") ) {
				pos_h = $(list).innerHeight();
				$(wrap).css("overflow", "hidden");
				$(wrap).css("height", pos_h+"px");

				tar_h = pos_h + $(title).innerHeight();
				for( var j = 0; j < item.length; j++ ) {
					tar_h += $(item[j]).innerHeight();
				}
				tar_h += $(info).innerHeight();
				$(wrap).animate( {"height":tar_h}, 250 );


				$(div).removeClass("item_detail_ticket_close");
				$(div).addClass("item_detail_ticket_open");

			}
			else {
				pos_h = $(list).innerHeight();
				$(wrap).animate( {"height":pos_h}, 250, function(){
					$(div).addClass("item_detail_ticket_close");
					$(div).removeClass("item_detail_ticket_open");
				} );
			}
		});
	};

	var entryTicket = function( div ) {
		var list = $(div).find(".item_detail_ticket_list");
		var url = $(div).find(".item_detail_ticket_table_open").find("a").attr("href");
		list.bind("click", function() {
			window.open(url, "_brank");
		});
	};

	var ticketInit = function() {

		var arr = $(".item_detail_ticket");
		if( arr ) {
			for( var i=0; i<arr.length; i++ ) {
				var url = $(arr[i]).find(".item_detail_ticket_table_open").find("a").attr("href");
				if( url ) {
					if( url == "javascript:void(0);" ) {
						entryButton( arr[i] );
					}
					else {
						entryTicket( arr[i] );
					}
				}
			}
		}

		var tip = $(".item_detail_ticket_table_item");
		if( tip ) {
			for( var i=0; i<tip.length; i++ ) {
				entryTip( tip[i] );
			}
		}
	};

	// ************************************************************************************
	// ************************************************************************************

	var photoChange = function( div )
	{
		var src = $(div).find("img").attr("src")

		var a = $("#item_detail_photo_main").find("a");
		$(a).find("img").attr( "src", src );
		
		$(".item_detail_photo_menu_cell_focus").css( "opacity", 0 );
		$(div).find(".item_detail_photo_menu_cell_focus").stop();
		$(div).find(".item_detail_photo_menu_cell_focus").clearQueue();
		$(div).find(".item_detail_photo_menu_cell_focus").animate( {"opacity":1}, 250 );
	};

	var photoEntryButton = function( div )
	{
		$(div).bind( "mouseover",	function(){
			$(div).stop();
			$(div).clearQueue();
			$(div).animate( {"opacity":0.50}, 250, "linear" );
		});
		$(div).bind( "mouseout",	function(){
			$(div).stop();
			$(div).clearQueue();
			$(div).animate( {"opacity":1.00}, 100, "linear" );
		});
		$(div).bind( "click",		function(){
			photoChange( div );
		});
	};

	var photoInit = function()
	{
		var arr = $(".item_detail_photo_menu_cell");
		if( arr )	{
			for( var i=0; i<arr.length; i++ ) {
				photoEntryButton( arr[i] );
			}
			photoChange( arr[0] );
		}

		var photoblock = $("#item_detail_photo_main");
		$(photoblock).bind( "mouseover",	function(){
			$(photoblock).css( "opacity", 0.5 );
			$(photoblock).stop();
			$(photoblock).clearQueue();
			$(photoblock).animate( {"opacity":1.00}, 500, "linear" );
		});

		$(photoblock).bind( "click",		function(){
			var a = $("#item_detail_photo_main").find("a");
			var imgsrc = $(a).find("img").attr( "src" );
			utility.imageView( imgsrc );
		});
	};

	// ************************************************************************************
	// ************************************************************************************

	jQuery.event.add( window, "load", function() {

		if( $("#related_area") ) {
			var relatedArea = new sliderObject("#related_area");
		}
		
		if( $("#other_area") ) {
			var otherArea = new sliderObject("#other_area");
		}
		
		ticketInit();
		photoInit();

		$("#map_bt_floating").find("a").bind("click", function(){
			$("#map_floating").stop();
			$("#map_floating").clearQueue();
			$("#map_floating").animate( {"opacity":0}, 200, function(){
				$("#map_floating").css( "display", "none" );
			} );
		});
	});

	jQuery.event.add( window, "resize",	function()	{	resize();	});

})();
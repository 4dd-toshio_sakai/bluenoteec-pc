(function(){
    var ua = navigator.userAgent;
        if (localStorage 
		 && !localStorage.getItem("sp_flag") 
		 && (ua.indexOf('iPhone') > 0 || ua.indexOf('iPod') > 0 || ua.indexOf('Android') > 0 ) || ua.indexOf('Mobile') > 0 ) {
            if(confirm('スマートフォン用サイトがあります。表示しますか？')) {
                var path=location.pathname;
                var target,count,pref,url;
                if(location.hostname=="www.bluenote.co.jp" || location.hostname=="bluenote.co.jp" || location.hostname=="mttest.bluenote.co.jp"){
                    //rule01 /jp/sp/
                    count=4;
                    pref='/jp/sp/';

                    //ページ遷移
                    target=path.slice( count );
                    location.href = pref+target+location.search;
			     }else if(location.hostname=="reserve.bluenote.co.jp" ){
				    //rule02 /reserve/
                    count=9;
				    pref='/reserve/mb_';

                    //ページ遷移
                    target=path.slice( count );
                    location.href = pref+target+location.search;
                }else if(location.hostname=="store.bluenote.co.jp" || location.hostname=="storetest.bluenote.co.jp"){
				    //rule03 /ec/
				    if(path.length==4){
					   count=4;
					   pref='/ec/mb_index';
				    }else if(path.indexOf("/ec/cd/")==0){
					   count=7;
					   pref='/ec/mb_cddvd/';
                    }else{
					   count=4;
					    pref='/ec/mb_';
                    }
				
                    //ページ遷移
                    target=path.slice( count );
                    location.href = pref+target+location.search;
			     }else{
                    //対象外
                    console.log('対象ドメインではありません。');
			     }
            }else{
                //NOを保存
                //localStorage.setItem("sp_flag",true);
            }
        }
})();
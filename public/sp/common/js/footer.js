var footer = (function()
{
	var init = function()
	{
		$("#pagetop").find("a").click( function(){
			$('html,body').animate({ scrollTop:0 }, 500 );
		});
	};

	// ******************************************

	jQuery.event.add( window, "load", function() {
		init();
	});

})();
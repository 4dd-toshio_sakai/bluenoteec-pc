function sliderObject( div ) 
{
	var objectNum = 1;
	var objectMax = 0;

	var entryPanel = function( div )
	{
		$(div).bind( "mouseover", function() {
			$(this).find(".item_panel_main_title").css("text-decoration", "underline" );
			$(this).find(".item_panel_sub_title").css("text-decoration", "underline" );
			$(this).find(".item_panel_pattern_img").css("opacity", 0.75 );
		});
		$(div).bind( "mouseout", function() {
			$(this).find(".item_panel_main_title").css("text-decoration", "none" );
			$(this).find(".item_panel_sub_title").css("text-decoration", "none" );
			$(this).find(".item_panel_pattern_img").css("opacity", 1.00 );
		});
	};

	var slide = function( num ) 
	{
		objectNum += num;

		var target = $(div);
		var dx = -960 * ( objectNum - 1 );
		var slider = $(target).find(".blockarea_contents_slider");
		$(slider).stop();
		$(slider).clearQueue();
		$(slider).animate( {"left":dx+"px" }, 250 );


		var state = objectNum + " / " + objectMax;
		$(target).find(".blockarea_navi_state").html( state ); 

		var pbt = $(target).find(".blockarea_navi_prev").find("a");
		if( objectNum == 1 )	{	
			$(pbt).stop();
			$(pbt).clearQueue();
			$(pbt).css("opacity",0.25);	
		}
		else 				{	
			$(pbt).stop();
			$(pbt).clearQueue();
			$(pbt).css("opacity",1.0);	
		}

		var nbt = $(target).find(".blockarea_navi_next").find("a");
		if( objectNum == objectMax )	{	
			$(nbt).stop();
			$(nbt).clearQueue();
			$(nbt).css("opacity",0.25);	
		}
		else 						{	
			$(nbt).stop();
			$(nbt).clearQueue();
			$(nbt).css("opacity",1.0);	
		}

		var page = $(target).find(".blockarea_contents_page");
		var th = $(page[objectNum-1]).height();
		$(target).find(".blockarea_contents").animate( {"height":th+"px" }, 500 );
	};

	var init = function() 
	{
		var target = $(div);
		var page = $(target).find(".blockarea_contents_page");
		if( page ) {
			objectMax = page.length;
			slide( 0 );

			var pbt = $(target).find(".blockarea_navi_prev").find("a");
			$(pbt).bind( "mouseover",	function(){
				if( objectNum != 1 ) {
					$(pbt).css("opacity",0.5);
					$(pbt).stop();
					$(pbt).clearQueue();
					$(pbt).animate( {"opacity":1.0}, 500 );
				}
			});
			$(pbt).bind( "click",		function(){
				if( objectNum != 1 ) {
					slide( -1 );
				}
			});

			var nbt = $(target).find(".blockarea_navi_next").find("a");
			$(nbt).bind( "mouseover",	function(){
				if( objectNum != objectMax ) {
					$(nbt).css("opacity",0.5);
					$(nbt).stop();
					$(nbt).clearQueue();
					$(nbt).animate( {"opacity":1.0}, 500 );
				}
			});
			$(nbt).bind( "click",		function(){
				if( objectNum != objectMax ) {
					slide( 1 );
				}
			});

			var slider = $(target).find(".blockarea_contents_slider");
			$(slider).css("width", objectMax*960+"px" );
		}


		var arr = $(target).find(".item_panel");
		for( var i=0; i<arr.length; i++ ) {
			entryPanel( arr[i] );
		}

	};

	init();
};
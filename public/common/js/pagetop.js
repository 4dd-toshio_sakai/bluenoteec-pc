var pagetop = (function()
{
	var init = function()
	{
		$(".pagetop").bind( "click", function(){
			$('html,body').animate( { "scrollTop":0 }, 250 );
		});
	};

	// ******************************************

	jQuery.event.add( window, "load", function() {
		init();
	});

})();